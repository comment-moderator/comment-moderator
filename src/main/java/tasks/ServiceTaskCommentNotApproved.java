package tasks;

import com.project.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Log
@Component
@AllArgsConstructor
@Scope(SCOPE_PROTOTYPE)
public class ServiceTaskCommentNotApproved implements JavaDelegate {
    private static final String COMMENT_NOT_APPROVED = "commentNotApproved";

    private final static String DECISION_ID = "notApproved";
    CommentService commentService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Execute comment not approved");

        log.info("Execute finish");
    }
}
