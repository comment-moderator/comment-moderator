package tasks;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.camunda.bpm.engine.delegate.DelegateTask;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Log
@Component
@AllArgsConstructor
@Scope(SCOPE_PROTOTYPE)
public class UserTaskSendCommentToModerate implements TaskListener {

    private static final String TASK_CONFIG_FILE = "UserTaskVerificationDocs";
    private static final String DECISION_ID = "userTaskVerificationDocsResult";

    public Boolean sendMessage(ApplicationContext applicationContext) {
        return null;
    }

    @Override
    public void notify(DelegateTask delegateTask) {

    }
}
