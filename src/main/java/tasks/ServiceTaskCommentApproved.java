package tasks;

import com.project.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Log
@Component
@AllArgsConstructor
@Scope(SCOPE_PROTOTYPE)
public class ServiceTaskCommentApproved implements JavaDelegate {
    private static final String COMMENT_APPROVED = "commentApproved";

    private final static String DECISION_ID = "approved";
    CommentService commentService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Execute comment approved");


        log.info("Execute finish");
    }
}
