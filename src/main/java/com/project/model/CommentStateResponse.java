package com.project.model;


import lombok.Builder;

@Builder
public class CommentStateResponse {

    Boolean successful;
    String comment;

}
