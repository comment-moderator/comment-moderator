package com.project.controller;

import com.project.model.CommentStateResponse;
import com.project.service.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tasks.UserTaskSendCommentToModerate;

@RestController
@Slf4j
public class CommentController {

    CommentService commentService;
    UserTaskSendCommentToModerate userTaskModerateComment;

    @GetMapping("messages/{messageId}")
    @Operation(description = "Получает id сообщения и формирует пользовательскую задачу")
    public ResponseEntity<CommentStateResponse> get(@PathVariable("messageId") String messageId) {
        commentService.getCommentContent(messageId);
        return null;
    }

}
